package az.expressbank.paydaytrade.service;

import az.expressbank.paydaytrade.dto.ResponseDto;
import az.expressbank.paydaytrade.dto.StatusListDto;
import az.expressbank.paydaytrade.dto.StockDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StockService {
    ResponseDto<List<StockDto>> getStockList();

    StatusListDto buyStock(String username, String stockName, String count);

    StatusListDto sellStock(String username, String stockName, String count);
}
