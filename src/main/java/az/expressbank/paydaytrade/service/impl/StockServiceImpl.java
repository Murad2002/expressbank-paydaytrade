package az.expressbank.paydaytrade.service.impl;

import az.expressbank.paydaytrade.dto.ResponseDto;
import az.expressbank.paydaytrade.dto.StatusDto;
import az.expressbank.paydaytrade.dto.StatusListDto;
import az.expressbank.paydaytrade.dto.StockDto;
import az.expressbank.paydaytrade.entity.Order;
import az.expressbank.paydaytrade.entity.Stock;
import az.expressbank.paydaytrade.entity.User;
import az.expressbank.paydaytrade.enums.StatusAndAccountConfirmed;
import az.expressbank.paydaytrade.exception.ExceptionConstants;
import az.expressbank.paydaytrade.exception.PaydayTradeException;
import az.expressbank.paydaytrade.repository.OrderRepository;
import az.expressbank.paydaytrade.repository.StockRepository;
import az.expressbank.paydaytrade.repository.UserRepository;
import az.expressbank.paydaytrade.service.StockService;
import az.expressbank.paydaytrade.util.Converter;
import az.expressbank.paydaytrade.util.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class StockServiceImpl implements StockService {

    private final StockRepository stockRepository;
    private final UserRepository userRepository;
    private final OrderRepository orderRepository;
    private final MailService mailService;

    @Override
    public ResponseDto<List<StockDto>> getStockList() {
        ResponseDto<List<StockDto>> response = new ResponseDto<>();
        List<StockDto> stockList = stockRepository.findAllByStatus(StatusAndAccountConfirmed.ACTIVE.getValue())
                .stream()
                .map(Converter::convertFromStock)
                .collect(Collectors.toList());
        response.setResponse(stockList);
        response.setStatusDto(StatusDto.getSuccessMessage());
        return response;
    }

    @Override
    public StatusListDto buyStock(String username, String stockName, String count) {
        StatusListDto statusListDto = new StatusListDto();
        User user = userRepository.findByUsernameAndStatus(username, StatusAndAccountConfirmed.ACTIVE.getValue());
        Stock stock = stockRepository.findByNameAndStatus(stockName, StatusAndAccountConfirmed.ACTIVE.getValue());
        Order order = orderRepository.findByUserAndStockAndStatus(user, stock, StatusAndAccountConfirmed.ACTIVE.getValue());
        try {
            if (username == null || stockName == null || count == null) {
                throw new PaydayTradeException(ExceptionConstants.INVALID_REQUEST_DATA, "Invalid Request Data !");
            }
            if (order == null) {
                if (Integer.valueOf(count) * stock.getPrice() > user.getBalance()) {
                    throw new PaydayTradeException(ExceptionConstants.INSUFFICIENT_FOUND, "Insufficient found !");
                }
                user.setBalance(user.getBalance() - (Integer.valueOf(count) * stock.getPrice()));
                userRepository.save(user);
                Order resultOrder = Order.builder()
                        .user(user)
                        .stock(stock)
                        .status(1)
                        .shareCount(Integer.valueOf(count))
                        .value(Integer.valueOf(count) * stock.getPrice())
                        .build();
                orderRepository.save(resultOrder);
                statusListDto.setStatusDto(StatusDto.getSuccessMessage());
                mailService.sendEmail(user.getEmail(), "Buy Order", "test.expressbank@gmail.com", "Stock Name: " + stock.getName() + ", Share Amount: " + count + " , Price: " + Integer.valueOf(count) * stock.getPrice());
            } else {
                if (Integer.valueOf(count) * stock.getPrice() > user.getBalance()) {
                    throw new PaydayTradeException(ExceptionConstants.INSUFFICIENT_FOUND, "Insufficient found !");
                }
                user.setBalance(user.getBalance() - (Integer.valueOf(count) * stock.getPrice()));
                userRepository.save(user);
                order.setValue((order.getShareCount() + Integer.valueOf(count)) * stock.getPrice());
                order.setShareCount(order.getShareCount() + Integer.valueOf(count));
                orderRepository.save(order);
                statusListDto.setStatusDto(StatusDto.getSuccessMessage());
                mailService.sendEmail(user.getEmail(), "Buy Order", "test.expressbank@gmail.com", "Stock Name: " + stock.getName() + ", Share Amount: " + count + " , Price: " + Integer.valueOf(count) * stock.getPrice());

            }


        } catch (PaydayTradeException ex) {
            statusListDto.setStatusDto(new StatusDto(ex.getCode(), ex.getMessage()));
        } catch (Exception ex) {
            ex.printStackTrace();
            statusListDto.setStatusDto(new StatusDto(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception !"));
        }
        return statusListDto;
    }

    @Override
    public StatusListDto sellStock(String username, String stockName, String count) {
        StatusListDto statusListDto = new StatusListDto();
        User user = userRepository.findByUsernameAndStatus(username, StatusAndAccountConfirmed.ACTIVE.getValue());
        Stock stock = stockRepository.findByNameAndStatus(stockName, StatusAndAccountConfirmed.ACTIVE.getValue());
        Order order = orderRepository.findByUserAndStockAndStatus(user, stock, StatusAndAccountConfirmed.ACTIVE.getValue());
        try {
            if (username == null || stockName == null || count == null) {
                throw new PaydayTradeException(ExceptionConstants.INVALID_REQUEST_DATA, "Invalid Request Data !");
            }
            if (order == null) {
                throw new PaydayTradeException(ExceptionConstants.INVALID_REQUEST_DATA, "Invalid Request Data!");
            } else {
                if (Integer.valueOf(count) > order.getShareCount()) {
                    throw new PaydayTradeException(ExceptionConstants.INSUFFICIENT_SHARE, "Insufficient share count !");
                }
                if (Integer.valueOf(count) == order.getShareCount()) {
                    user.setBalance(user.getBalance() + (Integer.valueOf(count) * stock.getPrice()));
                    userRepository.save(user);
                    order.setValue(Float.valueOf(0));
                    order.setShareCount(0);
                    orderRepository.save(order);
                    statusListDto.setStatusDto(StatusDto.getSuccessMessage());
                    mailService.sendEmail(user.getEmail(), "Sell Order", "test.expressbank@gmail.com", "Stock Name: " + stock.getName() + ", Share Amount: " + count + " , Price: " + Integer.valueOf(count) * stock.getPrice());
                } else {
                    user.setBalance(user.getBalance() + (Integer.valueOf(count) * stock.getPrice()));
                    userRepository.save(user);
                    order.setValue((order.getShareCount() - Integer.valueOf(count)) * stock.getPrice());
                    order.setShareCount(order.getShareCount() - Integer.valueOf(count));
                    orderRepository.save(order);
                    statusListDto.setStatusDto(StatusDto.getSuccessMessage());
                    mailService.sendEmail(user.getEmail(), "Sell Order", "test.expressbank@gmail.com", "Stock Name: " + stock.getName() + ", Share Amount: " + count + " , Price: " + Integer.valueOf(count) * stock.getPrice());
                }
            }


        } catch (PaydayTradeException ex) {
            statusListDto.setStatusDto(new StatusDto(ex.getCode(), ex.getMessage()));
        } catch (Exception ex) {
            ex.printStackTrace();
            statusListDto.setStatusDto(new StatusDto(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception !"));
        }
        return statusListDto;
    }
}
