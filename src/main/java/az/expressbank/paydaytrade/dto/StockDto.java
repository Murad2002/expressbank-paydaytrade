package az.expressbank.paydaytrade.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;

@Builder
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
public class StockDto {

    String name;
    Float price;
}
