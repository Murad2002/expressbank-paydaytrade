package az.expressbank.paydaytrade.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "CONFIRMATION_TOKENS")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ConfirmationToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "CONFIRMATION_TOKEN")
    String confirmationToken;

    @OneToOne
    @JoinColumn(nullable = false, name = "id")
    User user;

    @Column(name = "INSERTDATE")
    @CreationTimestamp
    LocalDateTime insertDate;

    public ConfirmationToken(User user) {
        confirmationToken = UUID.randomUUID().toString();
    }
}
