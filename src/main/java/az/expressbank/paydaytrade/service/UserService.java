package az.expressbank.paydaytrade.service;

import az.expressbank.paydaytrade.dto.StatusListDto;
import az.expressbank.paydaytrade.dto.UserDto;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

@Service
public interface UserService {

    StatusListDto signUp(UserDto userDto);

    ModelAndView confirmUserAccount(ModelAndView modelAndView,String confirmationToken);

    StatusListDto checkLogin(String identity, String password, String status);

    StatusListDto addMoney(String username, String amount);

    StatusListDto withdrawMoney(String username, String amount);
}
