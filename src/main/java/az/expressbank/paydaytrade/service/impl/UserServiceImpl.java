package az.expressbank.paydaytrade.service.impl;

import az.expressbank.paydaytrade.dto.StatusDto;
import az.expressbank.paydaytrade.dto.StatusListDto;
import az.expressbank.paydaytrade.dto.UserDto;
import az.expressbank.paydaytrade.entity.ConfirmationToken;
import az.expressbank.paydaytrade.entity.User;
import az.expressbank.paydaytrade.enums.StatusAndAccountConfirmed;
import az.expressbank.paydaytrade.exception.ExceptionConstants;
import az.expressbank.paydaytrade.exception.PaydayTradeException;
import az.expressbank.paydaytrade.repository.ConfirmationTokenRepository;
import az.expressbank.paydaytrade.repository.UserRepository;
import az.expressbank.paydaytrade.service.UserService;
import az.expressbank.paydaytrade.util.Converter;
import az.expressbank.paydaytrade.util.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.Transactional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@RequiredArgsConstructor
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ConfirmationTokenRepository confirmationTokenRepository;
    private final MailService mailService;

    @Override
    public StatusListDto signUp(UserDto userDto) {

        StatusListDto statusListDto = new StatusListDto();
        try {
            if (userDto.getName() == null || userDto.getSurname() == null || userDto.getEmail() == null || userDto.getUsername() == null || userDto.getPassword() == null) {
                throw new PaydayTradeException(ExceptionConstants.INVALID_REQUEST_DATA, "Invalid Request Data !");

            }
            if (!isValidPassword(userDto.getPassword())) {

                throw new PaydayTradeException(ExceptionConstants.INVALID_PASSWORD, "Invalid password !");
            }
            if (userRepository.findByEmailAndStatus(userDto.getEmail(), StatusAndAccountConfirmed.ACTIVE.getValue()) != null) {

                throw new PaydayTradeException(ExceptionConstants.USER_EMAIL_ALREADY_REGISTERED, "Email Already registered !");

            }
            if (userRepository.findByUsernameAndStatus(userDto.getUsername(), StatusAndAccountConfirmed.ACTIVE.getValue()) != null) {

                throw new PaydayTradeException(ExceptionConstants.USERNAME_ALREADY_REGISTERED, "Username Already Registered !");
            }

            statusListDto.setStatusDto(StatusDto.getSuccessMessage());

            User user = Converter.convertFromUserDto(userDto);
            userRepository.save(user);
            ConfirmationToken confirmationToken = new ConfirmationToken(user);
            String text = "To confirm your account, please click here : "
                    + "http://localhost:8082/paydaytrade/user/confirm-account?token=" + confirmationToken.getConfirmationToken();
            mailService.sendEmail(user.getEmail(), "Complete Registration!", "test.expressbank@gmail.com", text);

            confirmationTokenRepository.save(confirmationToken);
        } catch (PaydayTradeException ex) {
            statusListDto.setStatusDto(new StatusDto(ex.getCode(), ex.getMessage()));

        } catch (Exception ex) {
            ex.printStackTrace();
            statusListDto.setStatusDto(new StatusDto(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception !"));
        }

        return statusListDto;
    }

    @Override
    public ModelAndView confirmUserAccount(ModelAndView modelAndView, String confirmationToken) {
        ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);
        try {
            if (token != null) {
                User user = userRepository.findByEmailIgnoreCase(token.getUser().getEmail());
                user.setAccountConfirmed(StatusAndAccountConfirmed.ACTIVE.getValue());
                userRepository.save(user);
                modelAndView.setViewName("accountVerified.html");
            } else {
                modelAndView.addObject("message", "The link is invalid or broken!");
                modelAndView.setViewName("error");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return modelAndView;
    }

    @Override
    public StatusListDto checkLogin(String identity, String password, String status) {
        StatusListDto statusListDto = new StatusListDto();
        Integer statusCode = Integer.valueOf(status);
        try {
            if (identity == null || password == null || status == null) {
                throw new PaydayTradeException(ExceptionConstants.INVALID_REQUEST_DATA, "Invalid Request Data !");
            }
            if (statusCode.equals(1)) {
                if (userRepository.findByEmailAndAccountConfirmedAndStatus(identity, 0, 1) != null) {
                    throw new PaydayTradeException(ExceptionConstants.ACCOUNT_NOT_CONFIRMED, "Account has not been confirmed yet !");
                }
                if (userRepository.findByEmailAndAccountConfirmedAndStatus(identity, 1, 1) == null) {
                    throw new PaydayTradeException(ExceptionConstants.EMAIL_NOT_FOUND, "Wrong Email !");
                }

                if (userRepository.findByEmailAndPasswordAndAccountConfirmedAndStatus(identity, password, 1, 1) == null) {
                    throw new PaydayTradeException(ExceptionConstants.EMAIL_NOT_FOUND, "Wrong password !");
                }
                statusListDto.setStatusDto(StatusDto.getSuccessMessage());

            } else if (statusCode.equals(2)) {

                if (userRepository.findByUsernameAndAccountConfirmedAndStatus(identity, 0, 1) != null) {
                    throw new PaydayTradeException(ExceptionConstants.ACCOUNT_NOT_CONFIRMED, "Account has not been confirmed yet !");
                }

                if (userRepository.findByUsernameAndStatus(identity, 1) == null) {
                    throw new PaydayTradeException(ExceptionConstants.USERNAME_NOT_FOUND, "Wrong Username !");
                }

                if (userRepository.findByUsernameAndPasswordAndAccountConfirmedAndStatus(identity, password, 1, 1) == null) {
                    throw new PaydayTradeException(ExceptionConstants.USERNAME_NOT_FOUND, "Wrong password !");
                }
                statusListDto.setStatusDto(StatusDto.getSuccessMessage());

            } else {
                throw new PaydayTradeException(ExceptionConstants.INVALID_STATUS, "Invalid Status");
            }
        } catch (PaydayTradeException ex) {
            statusListDto.setStatusDto(new StatusDto(ex.getCode(), ex.getMessage()));

        } catch (Exception ex) {
            ex.printStackTrace();
            statusListDto.setStatusDto(new StatusDto(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception !"));
        }

        return statusListDto;
    }

    @Override
    public StatusListDto addMoney(String username, String amount) {
        StatusListDto statusListDto = new StatusListDto();
        User user = userRepository.findByUsernameAndStatus(username, StatusAndAccountConfirmed.ACTIVE.getValue());
        try {
            if (username == null || amount == null) {
                throw new PaydayTradeException(ExceptionConstants.INVALID_REQUEST_DATA, "Invalid Request Data !");
            }
            if (user == null) {
                throw new PaydayTradeException(ExceptionConstants.USERNAME_NOT_FOUND, "User not found !");
            }
            user.setBalance(user.getBalance() + Float.valueOf(amount));
            userRepository.save(user);
            statusListDto.setStatusDto(StatusDto.getSuccessMessage());
        } catch (PaydayTradeException ex) {
            statusListDto.setStatusDto(new StatusDto(ex.getCode(), ex.getMessage()));
        } catch (Exception ex) {
            ex.printStackTrace();
            statusListDto.setStatusDto(new StatusDto(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception !"));
        }
        return statusListDto;
    }

    @Override
    public StatusListDto withdrawMoney(String username, String amount) {
        StatusListDto statusListDto = new StatusListDto();
        User user = userRepository.findByUsernameAndStatus(username, StatusAndAccountConfirmed.ACTIVE.getValue());
        try {
            if (username == null || amount == null) {
                throw new PaydayTradeException(ExceptionConstants.INVALID_REQUEST_DATA, "Invalid Request Data !");
            }
            if (user == null) {
                throw new PaydayTradeException(ExceptionConstants.USERNAME_NOT_FOUND, "User not found !");
            }
            if(user.getBalance() < Float.valueOf(amount)){
                throw new PaydayTradeException(ExceptionConstants.INSUFFICIENT_FOUND,"Insufficient found !");
            }
            user.setBalance(user.getBalance() - Float.valueOf(amount));
            userRepository.save(user);
            statusListDto.setStatusDto(StatusDto.getSuccessMessage());
        } catch (PaydayTradeException ex) {
            statusListDto.setStatusDto(new StatusDto(ex.getCode(), ex.getMessage()));
        } catch (Exception ex) {
            ex.printStackTrace();
            statusListDto.setStatusDto(new StatusDto(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception !"));
        }
        return statusListDto;
    }


    public boolean isValidPassword(String password) {
        String regex = "^(?=.*[0-9])"
                + "(?=.*[a-z])(?=.*[A-Z])"
                + "(?=.*[@#$%^&+=_-])"
                + "(?=\\S+$).{6,20}$";

        Pattern p = Pattern.compile(regex);

        if (password == null) {
            return false;
        }

        Matcher m = p.matcher(password);

        return m.matches();
    }
}
