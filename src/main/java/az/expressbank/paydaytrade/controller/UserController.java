package az.expressbank.paydaytrade.controller;

import az.expressbank.paydaytrade.dto.StatusListDto;
import az.expressbank.paydaytrade.dto.UserDto;
import az.expressbank.paydaytrade.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/signUp")
    public StatusListDto signUp(@RequestBody UserDto userDto) {
        return userService.signUp(userDto);
    }

    @GetMapping(value = "/confirm-account")
    public ModelAndView confirmUserAccount(ModelAndView modelAndView,@RequestParam("token") String confirmationToken) {
        return userService.confirmUserAccount(modelAndView,confirmationToken);
    }


    @GetMapping("/login")
    public StatusListDto login(@RequestParam String identity, @RequestParam String password, @RequestParam String status){
        return userService.checkLogin(identity,password,status);
    }

    @PutMapping("/addmoney")
    public StatusListDto addMoney(@RequestParam String username, @RequestParam String amount){
        return userService.addMoney(username,amount);
    }

    @PutMapping("/withdrawmoney")
    public StatusListDto withdrawMoney(@RequestParam String username, @RequestParam String amount){
        return userService.withdrawMoney(username,amount);
    }




}
