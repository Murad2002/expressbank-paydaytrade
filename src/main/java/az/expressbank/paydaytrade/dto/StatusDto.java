package az.expressbank.paydaytrade.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StatusDto {

    private Integer code;
    private String  message;

    private static final Integer SUCCESS_CODE = 150;
    private static final String SUCCESS_MESSAGE = "success";


    public static StatusDto getSuccessMessage() {
        return new StatusDto(SUCCESS_CODE,SUCCESS_MESSAGE);
    }

}
