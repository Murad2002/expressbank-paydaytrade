package az.expressbank.paydaytrade.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "USERS")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "NAME")
    String name;

    @Column(name = "SURNAME")
    String surname;

    @Column(name = "EMAIL")
    String email;

    @Column(name = "USERNAME")
    String username;

    @Column(name = "PASSWORD")
    String password;

    @Column(name = "BALANCE")
    @ColumnDefault(value = "0")
    Float balance;

    @OneToOne(mappedBy = "user")
    ConfirmationToken confirmationToken;

    @Column(name = "STATUS")
    @ColumnDefault(value = "1")
    Integer status;

    @Column(name = "INSERTDATE")
    @CreationTimestamp
    LocalDateTime insertDate;

    @Column(name = "ACCOUNT_CONFIRMED")
    @ColumnDefault(value = "0")
    Integer accountConfirmed;
}
