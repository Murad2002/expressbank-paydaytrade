FROM alpine:3.11.2
RUN apk add --no-cache openjdk11
COPY build/libs/paydaytrade-0.0.1.war /app/app.war
WORKDIR /app
ENTRYPOINT ["java"]
CMD ["-jar","app.war"]
