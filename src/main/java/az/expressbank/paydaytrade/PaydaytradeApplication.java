package az.expressbank.paydaytrade;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class PaydaytradeApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaydaytradeApplication.class, args);
    }

}
