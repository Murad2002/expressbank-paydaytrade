package az.expressbank.paydaytrade.util;

import az.expressbank.paydaytrade.dto.StockDto;
import az.expressbank.paydaytrade.dto.UserDto;
import az.expressbank.paydaytrade.entity.Stock;
import az.expressbank.paydaytrade.entity.User;

public class Converter {
    public static User convertFromUserDto(UserDto userDto){
       return User.builder()
               .name(userDto.getName())
               .surname(userDto.getSurname())
               .email(userDto.getEmail())
               .username(userDto.getUsername())
               .password(userDto.getPassword())
               .status(1)
               .accountConfirmed(0)
               .balance(new Float(0.0))

               .build();


    }

    public static StockDto convertFromStock(Stock stock){
        return  StockDto.builder()
                .name(stock.getName())
                .price(stock.getPrice())
                .build();

    }
}
