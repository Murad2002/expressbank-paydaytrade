package az.expressbank.paydaytrade.repository;

import az.expressbank.paydaytrade.dto.StockDto;
import az.expressbank.paydaytrade.entity.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockRepository extends JpaRepository<Stock, Long> {

    List<Stock> findAllByStatus(Integer status);

    Stock findByNameAndStatus(String stockName, Integer status);
}
