package az.expressbank.paydaytrade.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "ORDERS")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    User user;

    @ManyToOne
    @JoinColumn(name = "stock_id", nullable = false)
    Stock stock;

    @Column(name = "SHARE_COUNT")
    @ColumnDefault(value = "1")
    Integer shareCount;

    @Column(name = "TOTAL_VALUE")
    Float value;

    @Column(name = "STATUS")
    @ColumnDefault(value = "1")
    Integer status;

    @Column(name = "INSERTDATE")
    @CreationTimestamp
    LocalDateTime insertDate;

}
