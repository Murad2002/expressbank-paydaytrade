package az.expressbank.paydaytrade.controller;

import az.expressbank.paydaytrade.dto.ResponseDto;
import az.expressbank.paydaytrade.dto.StatusListDto;
import az.expressbank.paydaytrade.dto.StockDto;
import az.expressbank.paydaytrade.service.StockService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/stock")
@RequiredArgsConstructor
public class StockController {
    private final StockService stockService;

    @GetMapping("/")
    public ResponseDto<List<StockDto>> geStockList() {
        return stockService.getStockList();
    }

    @PostMapping("/buyStock")
    public StatusListDto buyStock(@RequestParam String username, @RequestParam String stockName, @RequestParam String count) {
        return stockService.buyStock(username, stockName, count);
    }

    @PutMapping("/sellStock")
    public StatusListDto sellStock(@RequestParam String username, @RequestParam String stockName, @RequestParam String count) {
        return stockService.sellStock(username, stockName, count);
    }
}
