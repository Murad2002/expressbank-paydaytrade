package az.expressbank.paydaytrade.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "STOCKS")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Stock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "NAME")
    String name;

    @Column(name = "PRICE")
    Float price;

    @Column(name = "STATUS")
    @ColumnDefault(value = "1")
    Integer status;

    @Column(name = "INSERTDATE")
    @CreationTimestamp
    @ColumnDefault(value = "CURRENT_TIMESTAMP")
    LocalDateTime insertDate;
}
