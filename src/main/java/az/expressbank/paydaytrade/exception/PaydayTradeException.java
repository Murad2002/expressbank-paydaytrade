package az.expressbank.paydaytrade.exception;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class PaydayTradeException extends RuntimeException {

    private Integer code;

    public PaydayTradeException(Integer code, String message) {
         super(message);
         this.code = code;
    }

    public Integer getCode() {
        return code;
    }

}
