package az.expressbank.paydaytrade.exception;

public class ExceptionConstants {

    public static final Integer INTERNAL_EXCEPTION = 100;

    public static final Integer INVALID_REQUEST_DATA = 101;

    public static final Integer INVALID_PASSWORD = 102;

    public static final Integer USER_EMAIL_ALREADY_REGISTERED = 103;

    public static final Integer USERNAME_ALREADY_REGISTERED = 104;

    public static final Integer USERNAME_NOT_FOUND = 105;

    public static final Integer EMAIL_NOT_FOUND = 106;

    public static final Integer INVALID_STATUS = 107;

    public static final Integer ACCOUNT_NOT_CONFIRMED = 108;

    public static final Integer INSUFFICIENT_FOUND = 109;

    public static final Integer INSUFFICIENT_SHARE = 110;
}
