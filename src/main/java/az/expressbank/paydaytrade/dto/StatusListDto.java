package az.expressbank.paydaytrade.dto;

import lombok.Data;

@Data
public class StatusListDto {
    private StatusDto statusDto;
}
