package az.expressbank.paydaytrade.repository;

import az.expressbank.paydaytrade.entity.Order;
import az.expressbank.paydaytrade.entity.Stock;
import az.expressbank.paydaytrade.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order,Long> {
    Order findByUserAndStockAndStatus(User user , Stock stock, Integer status);
}
