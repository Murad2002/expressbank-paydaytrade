package az.expressbank.paydaytrade.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum StatusAndAccountConfirmed {

    ACTIVE(1),
    DEACTIVE(0);

    private final int value;



}
