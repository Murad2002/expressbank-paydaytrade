This application was created by using H2 database. (No need to add datasource)
I could not find stock api , so i created my table and added data (data automatically inserted when application run from import.sql)

All user stories have been created.

Main url : http://localhost:8082/paydaytrade
Swagger url : http://localhost:8082/paydaytrade/swagger-ui.html (Because of security problem Sign in windows come across , just cancel it two times. I ignored this url in security configuration and stil there is problem)

In order to build artifact to deploy the application to kubernetes cluster 
open terminal in root path of application (...\ExpressBank-PaydayTrade) 
Run command : docker build . -t {name of image} (Build Image)
Run command : docker run {name of image} (Create container)


First Sign Up and then test other APIs.
Sign up :
{
  "name": "Murad",
  "surname": "Gadirov",
  "email": "YOUR GMAIL",
  "username": "Murad",
  "password": "Murad1234-"
}

User Story 1 - sign up
url: http://localhost:8082/paydaytrade/user/signUp (POST)
This api accept Requestbody
Request
Example :
{
  "name": "Murad",
  "surname": "Gadirov",
  "email": "qadirovmurad2002@gmail.com",
  "username": "Murad",
  "password": "Murad1234-"
}
Response:
{
    "statusDto": {
        "code": 150,
        "message": "success"
    }
}

After sending request confirmation email is sent to gmail account (When it is clicked other api (http://localhost:8082/paydaytrade/user/confirm-account) activate account)

User Story 2 - sign in
url: http://localhost:8082/paydaytrade/user/login (GET)
Three parametrs is sent :
identity(username or email)
password
status (1- login with email 2- login with username)
Example:
http://localhost:8082/paydaytrade/user/login?identity=Murad&password=Murad1234-&status=2
Respone:
{
    "statusDto": {
        "code": 150,
        "message": "success"
    }
}

User story 3 - list stocks
url: http://localhost:8082/paydaytrade/stock/ (GET)
No parametr
Response:
{
    "response": [
        {
            "name": "10X CAPITAL VENTURE ACQUISITION",
            "price": 9.98
        }
        ... (Others)

User story 4 - deposit cash
url: http://localhost:8082/paydaytrade/user/addmoney?username=Murad&amount=200.65 (PUT)
It goes to users table and increase user balance

User story 5 - Place an order (Buy or Sell)
url (buy): http://localhost:8082/paydaytrade/stock/buyStock?username=Murad&stockName=3D SYSTEMS CORP&count=2 (POST)
url (sell): http://localhost:8082/paydaytrade/stock/sellStock?username=Murad&stockName=3D SYSTEMS CORP&count=1
(PUT)

User story 6 - Email verification
In the case of sell or buy details is sent to user email



Missing part: Unit tests 



